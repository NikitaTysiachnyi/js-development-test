class RectClass {
  constructor(x, y, w, h, status, id) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.status = status;
    this.id = id;
  }
  draw() {
    context.fillRect(this.x, this.y, this.w, this.h);
  }
}

const canvas = document.getElementById("canvas");
const context = canvas.getContext("2d");
const rect = [];
const snappingSize = 20;

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
canvas.style.backgroundColor = "#f3f3f3";

context.lineWidth = 3;
let point = {};
let previousPosition = {};
let warning = false;

const mouse = {
  x: 0,
  y: 0,
};

let selected = false;

// create rect
for (let i = 0; i < 2; i++) {
  rect.push(new RectClass(50, 50 + i * 80, 50, 50 + i * 10, false, i));
}

const createRect = () => {
  context.fillStyle = "#263375";
  context.clearRect(0, 0, window.innerWidth, window.innerHeight);

  rect.forEach((value, index) => {
    rect[index].draw();
  });
};

const debounce = (callback, wait) => {
  let timeoutId = null;
  return (...args) => {
    window.clearTimeout(timeoutId);
    timeoutId = window.setTimeout(() => {
      callback.apply(null, args);
    }, wait);
  };
};

const isCursorInRect = (rect) => {
  return (
    mouse.x > rect.x &&
    mouse.x < rect.x + rect.w &&
    mouse.y > rect.y &&
    mouse.y < rect.y + rect.h
  );
};

const checkCrossingByY = (selected, other) => {
  return selected.min < other.max || selected.max < other.min;
};

const checkCrossingByX = (selected, other) => {
  return selected.min < other.max || selected.max < other.min;
};

const findSnappingSide = (firstPoint, secondPoint) => {
  return Math.abs(firstPoint - secondPoint) <= snappingSize;
};

createRect();

// MOUSE MOVE
window.onmousemove = (e) => {
  debounce(createRect(), 100);
  mouse.x = e.pageX;
  mouse.y = e.pageY;
  if (selected) {
    selected.x = mouse.x - selected.w / 2;
    selected.y = mouse.y - selected.h / 2;
    getAllPoints();
    createRect();
  }
};

// MOUSE HOLD
window.onmousedown = () => {
  if (!selected) {
    rect.forEach((value, index) => {
      if (isCursorInRect(rect[index])) {
        previousPosition.x = rect[index].x;
        previousPosition.y = rect[index].y;
        rect[index].status = true;
        selected = rect[index];
        selected.index = index;
      }
    });
  }
};

// MOUSE LOST
window.onmouseup = () => {
  // snapping();
  if (selected) {
    createRect();
    rect[selected.index].status = false;
    selected = false;
  }
};
// Y - TOP
// X - LEFT
function getAllPoints() {
  const selectedXAll = [];
  const selectedYAll = [];
  const selectedRect = [
    { x: selected.x, y: selected.y },
    { x: selected.x + selected.w, y: selected.y },
    { x: selected.x, y: selected.y + selected.h },
    { x: selected.x + selected.w, y: selected.y + selected.h },
  ];
  selectedRect.forEach((value) => {
    selectedXAll.push(value.x);
    selectedYAll.push(value.y);
  });

  if (selected) {
    const notSelectedRect = rect.filter((value) => !value.status);
    const notSelectedXAll = [];
    const notSelectedYAll = [];
    notSelectedRect.forEach((value) => {
      const currentRect = [
        { x: value.x, y: value.y },
        { x: value.x + value.w, y: value.y },
        { x: value.x, y: value.y + value.h },
        { x: value.x + value.w, y: value.y + value.h },
      ];
      currentRect.forEach((value) => {
        notSelectedXAll.push(value.x);
        notSelectedYAll.push(value.y);
      });
    });
    const notSelectedX = {
      max: Math.max(...notSelectedXAll),
      min: Math.min(...notSelectedXAll),
    };
    const notSelectedY = {
      max: Math.max(...notSelectedYAll),
      min: Math.min(...notSelectedYAll),
    };
    const selectedX = {
      max: Math.max(...selectedXAll),
      min: Math.min(...selectedXAll),
    };
    const selectedY = {
      max: Math.max(...selectedYAll),
      min: Math.min(...selectedYAll),
    };
    if (
      (findSnappingSide(notSelectedX.min, selectedX.max) &&
        checkCrossingByY(selectedY, notSelectedY)) ||
      (findSnappingSide(notSelectedX.max, selectedX.min) &&
        checkCrossingByY(selectedY, notSelectedY))
    ) {
      const dist = [
        notSelectedX.min - selectedX.max,
        notSelectedX.max - selectedX.min,
      ];
      const valueArr = [];

      dist.filter((value) =>
        value <= snappingSize && value >= -snappingSize
          ? valueArr.push(value)
          : false
      );
      valueArr.length >= 1 ? (selected.x += valueArr[0]) : "";
    } else if (
      (findSnappingSide(notSelectedY.min, selectedY.max) &&
        checkCrossingByX(selectedX, notSelectedX)) ||
      (findSnappingSide(notSelectedY.max, selectedY.min) &&
        checkCrossingByX(selectedX, notSelectedX))
    ) {
      const dist = [
        notSelectedY.min - selectedY.max,
        notSelectedY.max - selectedY.min,
      ];
      const valueArr = [];

      dist.filter((value) =>
        value <= snappingSize && value >= -snappingSize
          ? valueArr.push(value)
          : false
      );
      valueArr.length >= 1 ? (selected.y += valueArr[0]) : "";
    }
  }
}
